NJOBS=500  # The number of jobs for proton-proton simulations

code=RunProtonProtonFastJetEff # The executable you want to run copies of
output=pytreeEff.root          # The output root tree name in each folder. Temporary to be moved to output path
outputpath=/usatlas/u/pyin/workarea/PYTHIA8/SimulationCode/PYTHIA8_output/proton_proton # Where all the results will end up

rm code/$code # Get rid of an old executable, if it exists
g++ code/$code.cc -o code/$code -pedantic -W -Wall -Wshadow -fPIC -I$PYTHIA8/include -L$PYTHIA8/lib -lpythia8 -ldl `root-config --libs --cflags` `fastjet-config --cxxflags --libs --plugins` # Compile code with all necessary options. Current options are Pythia-8, fastjet, and soft-drop

rm -rf $outputpath    # If the output folder already existed, trash it.
mkdir -p $outputpath  # Make the output folder
mkdir -p $outputpath/TestR4
# mkdir -p pp_jobs

file=c_job.sub        # Definition of the file to contain the condor job instructions

rm -f $file           # If the condor file already existed, delete it.

echo "Universe        = vanilla" >>$file
echo "Priority        = +20"  >>$file
echo "GetEnv          = true" >>$file
echo "Executable      = \$(Initialdir)/run.sh" >>$file

# Now, we will loop over all the jobs to set up the folder for its execution and add it to the condor job file
for job in $(seq 1 $NJOBS)
do
  rm -rf pp_jobs/job$job         # If the job folder already existed, delete it
  mkdir pp_jobs/job$job          # Make a new job folder
  cp code/$code pp_jobs/job$job  # Copy the code executable into that folder

  dir=$(pwd)/pp_jobs/job$job     # The directory that the job executes in
  echo "Output          = /usatlas/scratch/pyin/log/c_job$job.sub.out" >>$file  # Output location for the job's plain text IO
  echo "Error           = /usatlas/scratch/pyin/log/c_job$job.sub.err" >>$file  # Output location for the job's error IO
  echo "Log             = /usatlas/scratch/pyin/log/c_job$job.sub.log" >>$file  # Output location for condor's exec info
  echo "Initialdir      = $dir" >>$file       # Set the different folders for job executions
  echo "Arguments       = \"$job \"" >>$file  # Set the different arguments (random seeds)
  echo "queue" >>$file                        # The job is good to go and condor will queue it

  # Now for the fun job of using a bash script to write a bash script
  echo "#!/bin/bash">>pp_jobs/job$job/run.sh          # Set up the bash file which will run your code
  echo "source /usatlas/u/pyin/workarea/PYTHIA8/setup.sh">>pp_jobs/job$job/run.sh          # Source your setup file to make sure all requisite libraries are available
  echo "./$code \${1}">>pp_jobs/job$job/run.sh        # Execute the code with the argument condor provides
  echo "mv $output $outputpath/TestR4/pytree_\${1}.root">>pp_jobs/job$job/run.sh # Move the output tree to the right spot
  echo "Done making proton-proton job $job"
  chmod a+x pp_jobs/job$job/run.sh     # Make the script we just wrote executable
done

condor_submit $file  # All done! Let condor work its magic.
