#include <cmath>
#include <fstream>
#include <string>
#include <utility>

#include "TMath.h"
#include "TFile.h"
#include "TTree.h"
#include "TF1.h"
#include "TChain.h"
#include "TH1.h"
#include "TH2.h"
#include "TH3.h"
#include "DatasetEnums.h"
#include "bins.h"
#include "TLorentzVector.h"
#include "fastjet/ClusterSequence.hh"

using namespace fastjet;

static TFile *tmpf;
static TFile *treeOutFile;

int EtaBins3D = 8;
int PhiBins3D = 8;

bool inFolder = 1;
string filePrefix = "pytree_MONASH13_HardQCD_noMPIISR_100M";

struct particle {
    int    id ;
    double m  ;
    double eta;
    double phi;
    double pT ;
};
vector<particle> final_p;

void GlobalEtaPhiCondor(int fileIndex);
void InitTree(int fileIndex);
void InitHistos(int fileIndex);
void SaveHistos();
void ReconstructJet();
int GetEventNch();
int GetMuliBinIndex(float nch);
double ConvertPhi(double phi);

TChain *fChain;
vector<int>    *particle_id  = 0;
vector<double> *particle_pt  = 0;
vector<double> *particle_eta = 0;
vector<double> *particle_phi = 0;
vector<double> *particle_m   = 0;
vector<double> *particle_tau = 0;

TTree *jetTree;

float reco_jet_pt ;
float reco_jet_eta;
float reco_jet_phi;
float reco_jet_Nch;
vector<vector<float>> *reco_jet_Constituent;

int event_counter[Bins::NCENT] = {0};

TH1* h1_ntrk  ;
TH1* h1_pT    ;
TH1* h1_eta   ;
TH1* h1_tau   ;
TH1* h1_jetpt ;
TH1* h1_jetNch;
TH1* h_etaGap   [Bins::NCENT];
TH1* h_sumetaGap[Bins::NCENT];

TH2* h2_jetEtaPhi;
TH2* h2_NumPT_EbyE[Bins::NCENT][8];

TH3* h3_EtaPhi_EbyE         [Bins::NCENT];
TH3* h3_EtaPhi_ptWeight_EbyE[Bins::NCENT];
TH3* h3_EtaPhiPt            [Bins::NCENT];
TH3* h3_EtaPhi_jetNch[10];

void GlobalEtaPhiCondor(int fileIndex) {
    InitTree(fileIndex);
    InitHistos(fileIndex);

    int entries = fChain->GetEntries();

    /*-----------------------------------------------------------------------------
     *  Start loop over all events
     *-----------------------------------------------------------------------------*/
    for (int i = 0; i < entries; i++) {
        final_p.clear();

        if (i > 1) {
            float log10Entry = floor(log (i) / log(10));
            int   modEntry   = i % (int) pow(10, log10Entry);

            if (modEntry == 0) std::cout << "Processed event " << i << std::endl;
        }

        int nbytes = fChain->GetEntry(i);
        if (nbytes <= 0) {std::cout << "read in 0 bytes" << std::endl; break;}

        //Nch used for multiplicity evaluation
        double nch = GetEventNch();

        if (nch >= 200 || nch < 1) continue;

        //Determine multiplicity bin
        int m_mult_i = GetMuliBinIndex(nch);
        if (m_mult_i < 0 || m_mult_i >= Bins::NCENT) continue;

        ReconstructJet();

        h1_ntrk->Fill(nch);
        event_counter[m_mult_i] += 1;

        std::unique_ptr<TH2D> temp_etaphi          {new TH2D("temp_etaphi         ", "", EtaBins3D, -2.5, 2.5, PhiBins3D, -acos(-1.0), acos(-1.0))};
        std::unique_ptr<TH2D> temp_etaphi_ptWeight {new TH2D("temp_etaphi_ptWeight", "", EtaBins3D, -2.5, 2.5, PhiBins3D, -acos(-1.0), acos(-1.0))};

        // -----------------------------------------------------------------------------
        //    Information about tracks in the event
        // -----------------------------------------------------------------------------
        int eventSize = final_p.size();
        vector<float> particle_eta;
        for (int j = 0; j < eventSize; j++) {
            double pt  = final_p[j].pT ;
            double eta = final_p[j].eta;
            double phi = final_p[j].phi;

            particle_eta.push_back(eta);

            h3_EtaPhiPt[m_mult_i]->Fill(eta, phi, pt);

            temp_etaphi         ->Fill(eta, phi);
            temp_etaphi_ptWeight->Fill(eta, phi, pt);

            h1_pT ->Fill(pt );
            h1_eta->Fill(eta);
        }

        // -----------------------------------------------------------------------------
        // Calculate eta gap
        //
        particle_eta.push_back( 2.5);
        particle_eta.push_back(-2.5);
        std::sort(particle_eta.begin(), particle_eta.end());
        double sumEtaGap  = 0;
        if (particle_eta.size() > 2) {
            for (int ipar = 0; ipar < (int) particle_eta.size() - 1; ipar++) {
                float deta = particle_eta[ipar + 1] - particle_eta[ipar];
                h_etaGap[m_mult_i]->Fill(deta);
                if (deta > 1) {sumEtaGap += deta;}
            }
            h_sumetaGap[m_mult_i]->Fill(sumEtaGap);
        }
        // -----------------------------------------------------------------------------

        for (int ieta = 1; ieta <= EtaBins3D; ieta++) {
            double eta = temp_etaphi->GetXaxis()->GetBinCenter(ieta);
            float avgBinPT  = 0.0;
            float avgBinNch = 0.0;
            for (int jphi = 1; jphi <= PhiBins3D; jphi++) {
                double phi = temp_etaphi->GetYaxis()->GetBinCenter(jphi);
                h3_EtaPhi_EbyE         [m_mult_i]->Fill(eta, phi, temp_etaphi         ->GetBinContent(ieta, jphi));
                h3_EtaPhi_ptWeight_EbyE[m_mult_i]->Fill(eta, phi, temp_etaphi_ptWeight->GetBinContent(ieta, jphi));

                avgBinPT  += temp_etaphi_ptWeight->GetBinContent(ieta, jphi);
                avgBinNch += temp_etaphi         ->GetBinContent(ieta, jphi);
            }
            avgBinPT  /= PhiBins3D;
            avgBinNch /= PhiBins3D;
            h2_NumPT_EbyE[m_mult_i][ieta - 1]->Fill(avgBinPT, avgBinNch);
        }
    }

    for (int icent = 0; icent < Bins::NCENT; icent++) {
        // h3_EtaPhiPt[icent]->Scale(1. / event_counter[icent]);
        h3_EtaPhiPt[icent]->Scale(1. / h3_EtaPhiPt[icent]->GetXaxis()->GetBinWidth(1));
        h3_EtaPhiPt[icent]->Scale(1. / h3_EtaPhiPt[icent]->GetYaxis()->GetBinWidth(1));
        h3_EtaPhiPt[icent]->Scale(1. / h3_EtaPhiPt[icent]->GetZaxis()->GetBinWidth(1));


        h3_EtaPhi_EbyE[icent]->Scale(1. / h3_EtaPhi_EbyE[icent]->GetXaxis()->GetBinWidth(1));
        h3_EtaPhi_EbyE[icent]->Scale(1. / h3_EtaPhi_EbyE[icent]->GetYaxis()->GetBinWidth(1));
        h3_EtaPhi_EbyE[icent]->Scale(1. / h3_EtaPhi_EbyE[icent]->GetZaxis()->GetBinWidth(1));
        h3_EtaPhi_ptWeight_EbyE[icent]->Scale(1. / h3_EtaPhi_ptWeight_EbyE[icent]->GetXaxis()->GetBinWidth(1));
        h3_EtaPhi_ptWeight_EbyE[icent]->Scale(1. / h3_EtaPhi_ptWeight_EbyE[icent]->GetYaxis()->GetBinWidth(1));
        h3_EtaPhi_ptWeight_EbyE[icent]->Scale(1. / h3_EtaPhi_ptWeight_EbyE[icent]->GetZaxis()->GetBinWidth(1));

        for (int ieta = 1; ieta <= EtaBins3D; ieta++) {
            h2_NumPT_EbyE[icent][ieta - 1]->Scale(1. / h2_NumPT_EbyE[icent][ieta - 1]->GetXaxis()->GetBinWidth(1));
            h2_NumPT_EbyE[icent][ieta - 1]->Scale(1. / h2_NumPT_EbyE[icent][ieta - 1]->GetYaxis()->GetBinWidth(1));
        }
    }
    std::cout << "Finish making global eta-phi plots" << std::endl;
    SaveHistos();

    return;
}

void ReconstructJet() {
    vector<PseudoJet> particles;
    int nch = final_p.size();
    for (int j = 0; j < nch; j++) {
        double pt  = final_p[j].pT ;
        double eta = final_p[j].eta;
        double phi = final_p[j].phi;
        double m   = final_p[j].m  ;

        TLorentzVector v;
        // v.SetPtEtaPhiM(pt, eta, phi, m);
        v.SetPtEtaPhiM(pt, eta, phi, 0);

        particles.push_back(PseudoJet(v.Px(), v.Py(), v.Pz(), v.E()));
    }

    // choose a jet definition
    double R = 0.4;
    JetDefinition jet_def(antikt_algorithm, R);

    // run the clustering, extract the jets
    ClusterSequence cs(particles, jet_def);
    vector<PseudoJet> jets = sorted_by_pt(cs.inclusive_jets());

    for (unsigned i = 0; i < jets.size(); i++) {
        reco_jet_pt  = 0;
        reco_jet_eta = 0;
        reco_jet_phi = 0;
        reco_jet_Nch = 0;
        reco_jet_Constituent->clear();

        vector<PseudoJet> constituents = jets[i].constituents();
        double jetpt  = jets[i].pt();
        double jeteta = jets[i].eta();
        double jetphi = ConvertPhi(jets[i].phi());

        if (jetpt >= 10.0) {
            h1_jetpt->Fill(jetpt);
            h1_jetNch->Fill(constituents.size());
            h2_jetEtaPhi->Fill(jeteta, jetphi);

            reco_jet_pt  = jetpt ;
            reco_jet_eta = jeteta;
            reco_jet_phi = jetphi;
            reco_jet_Nch = constituents.size();

            for (int j = 0; j < (int) constituents.size(); j++) {
                vector<float> jetParticle;
                jetParticle.push_back(constituents[j].pt());
                jetParticle.push_back(constituents[j].eta());
                jetParticle.push_back(ConvertPhi(constituents[j].phi()));
                reco_jet_Constituent->push_back(jetParticle);
            }

            if      (jetpt <  10) h3_EtaPhi_jetNch[0]->Fill(jeteta, jetphi, constituents.size());
            else if (jetpt <  20) h3_EtaPhi_jetNch[1]->Fill(jeteta, jetphi, constituents.size());
            else if (jetpt <  30) h3_EtaPhi_jetNch[2]->Fill(jeteta, jetphi, constituents.size());
            else if (jetpt <  40) h3_EtaPhi_jetNch[3]->Fill(jeteta, jetphi, constituents.size());
            else if (jetpt <  50) h3_EtaPhi_jetNch[4]->Fill(jeteta, jetphi, constituents.size());
            else if (jetpt <  60) h3_EtaPhi_jetNch[5]->Fill(jeteta, jetphi, constituents.size());
            else if (jetpt <  70) h3_EtaPhi_jetNch[6]->Fill(jeteta, jetphi, constituents.size());
            else if (jetpt <  80) h3_EtaPhi_jetNch[7]->Fill(jeteta, jetphi, constituents.size());
            else if (jetpt <  90) h3_EtaPhi_jetNch[8]->Fill(jeteta, jetphi, constituents.size());
            else                  h3_EtaPhi_jetNch[9]->Fill(jeteta, jetphi, constituents.size());

            jetTree->Fill();
        }
    }
}

int GetEventNch() {
    int eventSize = (*particle_id).size();
    int ntrk = 0;
    for (int j = 0; j < eventSize; j++) {
        int partid = (*particle_id)[j];
        if (abs(partid) != 211 && abs(partid) != 321 && abs(partid) != 2212) continue;
        double pT  = (*particle_pt )[j];
        double eta = (*particle_eta)[j];
        double phi = (*particle_phi)[j]; phi = ConvertPhi(phi);
        double tau = (*particle_tau)[j];
        tau *= 1E9 / TMath::C();
        if (fabs(eta) > 2.5) continue;
        if (       pT < 0.4) continue;
        h1_tau->Fill(tau);
        if (tau > 0 && tau < 300) continue;

        particle p;
        p.pT  = pT ;
        p.eta = eta;
        p.phi = phi;

        final_p.push_back(p);
        ntrk++;
    }
    return ntrk;
}


//nch based binning
int GetMuliBinIndex(float nch) {
    for (int i = 0; i < Bins::NCENT; i++) {
        if (nch >= (Bins::NTRACK_PPB_LO[i] - .0001)) {
            return i;
        }
    }
    return -1;
}


double ConvertPhi(double phi) {
    if (phi >  TMath::Pi()) phi = phi - 2 * TMath::Pi();
    if (phi < -TMath::Pi()) phi = phi + 2 * TMath::Pi();
    return phi;
}


void InitTree(int fileIndex) {
    char name[600];
    fChain = new TChain("PyTree");
    if (inFolder) sprintf(name, "01RootFiles/%s/%s_%d.root", filePrefix.c_str(), filePrefix.c_str(), fileIndex);
    else          sprintf(name, "01RootFiles/%s.root"  , filePrefix.c_str());
    fChain->Add(name);
    fChain->SetBranchAddress("particle_id" , &particle_id );
    fChain->SetBranchAddress("particle_pt" , &particle_pt );
    fChain->SetBranchAddress("particle_eta", &particle_eta);
    fChain->SetBranchAddress("particle_phi", &particle_phi);
    fChain->SetBranchAddress("particle_m"  , &particle_m  );
    fChain->SetBranchAddress("particle_tau", &particle_tau);
}

void InitHistos(int fileIndex) {
    char histname[600], histtitle[600];

    /*-----------------------------------------------------------------------------
     *  Create the output TFile
     *-----------------------------------------------------------------------------*/
    char name[600] = "";
    sprintf(name, "03GlobalEtaPhi/%s_globalEtaPhi_%d.root", filePrefix.c_str(), fileIndex);
    tmpf = new TFile(name, "recreate");

    h1_ntrk   = new TH1D("h1_ntrk"  , "h1_ntrk;N_{ch};N"  ,  200, -0.5, 199.5);
    h1_pT     = new TH1D("h1_pT"    , "h1_pT;p_{T};N"     , 1000,  0  , 100  );
    h1_eta    = new TH1D("h1_eta"   , "h1_eta;#eta;N"     ,   50, -2.5,   2.5);
    h1_tau    = new TH1D("h1_tau"   , "h1_tau;#tau;N"     , 5000,  0  , 5000 );
    h1_jetpt  = new TH1D("h1_jetpt" , "h1_jetpt;p_{T};N"  , 2000,  0  ,  200 );
    h1_jetNch = new TH1D("h1_jetNch", "h1_jetNch;N_{ch};N",   50,  0  ,   50 );
    h2_jetEtaPhi = new TH2D("h2_jetEtaPhi", "h2_jetEtaPhi;#eta;#phi;N", 50, -2.5, 2.5, 64, -acos(-1.0), acos(-1.0));

    /*-----------------------------------------------------------------------------
     *  3D Eta-phi map for convolution study
     *
     *-----------------------------------------------------------------------------*/
    for (int i = 0; i < 10; i++) {
        sprintf(histname , "h3_EtaPhi_jetNch_jetPt%d_%d", i * 10, i * 10 + 10);
        sprintf(histtitle, "h3_EtaPhi_jetNch;#eta;#phi;N_{ch}");
        h3_EtaPhi_jetNch[i] = new TH3D(histname, histtitle, EtaBins3D, -2.5, 2.5, PhiBins3D, -acos(-1.0), acos(-1.0), 50, 0.0, 50.0);
    }


    for (int icent = 0; icent < Bins::NCENT; icent++) {
        /*-----------------------------------------------------------------------------
         *  Eta gap
         *
         *-----------------------------------------------------------------------------*/
        sprintf(histname , "h_etaGap_cent%d", icent);
        sprintf(histtitle, "h_etaGap;#Delta#eta;count");
        h_etaGap[icent] = new TH1D(histname, histtitle, 100, 0.0, 5.0);

        /*-----------------------------------------------------------------------------
         *  Sum eta gap
         *
         *-----------------------------------------------------------------------------*/
        sprintf(histname , "h_sumetaGap_cent%d", icent);
        sprintf(histtitle, "h_sumetaGap;#Delta#eta;count");
        h_sumetaGap[icent] = new TH1D(histname, histtitle, 100, 0.0, 5.0);


        /*-----------------------------------------------------------------------------
         *  3D Eta-phi map for event by event fluctuation study
         *
         *-----------------------------------------------------------------------------*/
        sprintf(histname , "h3_EtaPhi_EbyE_cent%d", icent);
        sprintf(histtitle, "h3_EtaPhi_EbyE;#eta;#phi;N_{Tracks}");
        h3_EtaPhi_EbyE[icent] = new TH3D(histname, histtitle, EtaBins3D, -2.5, 2.5, PhiBins3D, -acos(-1.0), acos(-1.0), 50, -0.5, 49.5);

        /*-----------------------------------------------------------------------------
        *  Eta-phi with pT weight global map for event by event fluctuation study
        *
        *-----------------------------------------------------------------------------*/
        sprintf(histname , "h3_EtaPhi_ptWeight_EbyE_cent%d", icent);
        sprintf(histtitle, "h3_EtaPhi_ptWeight_EbyE;#eta;#phi;p_{T}");
        h3_EtaPhi_ptWeight_EbyE[icent] = new TH3D(histname, histtitle, EtaBins3D, -2.5, 2.5, PhiBins3D, -acos(-1.0), acos(-1.0), 500, 0.0, 100.0);

        /*-----------------------------------------------------------------------------
        *  Eta-phi-pT pT spectrum in each bin
        *
        *-----------------------------------------------------------------------------*/
        sprintf(histname , "h3_EtaPhiPt_cent%d", icent);
        sprintf(histtitle, "h3_EtaPhiPt;#eta;#phi;p_{T}");
        h3_EtaPhiPt[icent] = new TH3D(histname, histtitle, EtaBins3D, -2.5, 2.5, PhiBins3D, -acos(-1.0), acos(-1.0), 200, 0.0, 20.0);

        /*-----------------------------------------------------------------------------
        *  Number of particles vs pT in each phi:eta bin
        *
        *-----------------------------------------------------------------------------*/
        for (int ieta = 0; ieta < EtaBins3D; ieta++) {
            sprintf(histname , "h2_NumPT_EbyE_cent%d_etaBin%d", icent, ieta);
            sprintf(histtitle, "h2_NumPT_EbyE;p_{T};N_{ch};");
            h2_NumPT_EbyE[icent][ieta] = new TH2D(histname, histtitle, 200, 0.0, 20.0, 160, 0.0, 20.0);
        }
    }


    sprintf(name, "03GlobalEtaPhi/%s_jetTree_%d.root", filePrefix.c_str(), fileIndex);
    treeOutFile = new TFile(name, "recreate");

    jetTree = new TTree("jetTree", "jetTree");
    jetTree->Branch("reco_jet_pt" , &reco_jet_pt );
    jetTree->Branch("reco_jet_eta", &reco_jet_eta);
    jetTree->Branch("reco_jet_phi", &reco_jet_phi);
    jetTree->Branch("reco_jet_Nch", &reco_jet_Nch);
    jetTree->Branch("reco_jet_Constituent", &reco_jet_Constituent);
}

void SaveHistos() {
    treeOutFile->cd();
    jetTree->Write();
    std::cout << "Saving jet tree finished" << std::endl;
    delete treeOutFile;
    treeOutFile = 0;

    tmpf->cd();
    tmpf->Write();
    std::cout << "Saving Finished" << std::endl;
    delete tmpf;
    tmpf = 0;
}

