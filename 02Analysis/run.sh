#!/bin/zsh

# ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
# source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh ""
# asetup 21.2,latest,AnalysisBase
# localSetupROOT 6.16.00-x86_64-slc6-gcc62-opt


echo "Starting the run"
root -b -l <<EOF
  gSystem->Load("libfastjet.so")
  .L AnaCorrPythia8.C+

  AnaCorrPythia8* agp = new AnaCorrPythia8();
  agp->run();
  .q;
EOF
# tot=`echo $1 | sed -e 's/-//g'`
# file=03Done/done_tot${tot}_from${2}_to${3}_datatype${4}_eff${5}_trig${6}_pileupreject${7}_jetrejecttype${8}_jetrejectcut${9}_corrtype${10}_multtype${11}.txt
# touch $file
echo "DONE RUNNING THE SCRIPT"
