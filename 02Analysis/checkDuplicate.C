#include <cmath>
#include <fstream>
#include <string>
#include <utility>

#include "TMath.h"
#include "TFile.h"
#include "TTree.h"
#include "TF1.h"
#include "TChain.h"
#include "TH1.h"
#include "TH2.h"
#include "TH3.h"
#include "DatasetEnums.h"
#include "bins.h"
#include "TLorentzVector.h"
#include "fastjet/ClusterSequence.hh"

using namespace fastjet;

static TFile *tmpf;


int EtaBins3D = 8;
int PhiBins3D = 8;

bool inFolder = 0;
string filePrefix = "largeseed_876543210";

struct particle {
    int    id ;
    double m  ;
    double eta;
    double phi;
    double pT ;
};
vector<particle> final_p;

void GlobalEtaPhi();
void InitTree();
void InitHistos();
void SaveHistos();
void ReconstructJet();
int GetEventNch();
int GetMuliBinIndex(float nch);
double ConvertPhi(double phi);

TChain *fChain;
vector<int>    *particle_id  = 0;
vector<double> *particle_pt  = 0;
vector<double> *particle_eta = 0;
vector<double> *particle_phi = 0;
vector<double> *particle_m   = 0;
vector<double> *particle_tau = 0;

TH1* h1_ntrk ;
TH1* h1_tau  ;
TH1* h1_jetpt;

TH2* h2_jetEtaPhi;

TH3* h3_EtaPhi_jetNch[10];

void checkDuplicate() {
    InitTree();
    InitHistos();

    int entries = fChain->GetEntries();

    /*-----------------------------------------------------------------------------
     *  Start loop over all events
     *-----------------------------------------------------------------------------*/
    for (int i = 0; i < entries; i++) {
        final_p.clear();

        if (i > 1) {
            float log10Entry = floor(log (i) / log(10));
            int   modEntry   = i % (int) pow(10, log10Entry);

            if (modEntry == 0) std::cout << "Processed event " << i << std::endl;
        }

        int nbytes = fChain->GetEntry(i);
        if (nbytes <= 0) {std::cout << "read in 0 bytes" << std::endl; break;}

        //Nch used for multiplicity evaluation
        double nch = GetEventNch();

        if (nch >= 200 || nch < 1) continue;

        //Determine multiplicity bin
        int m_mult_i = GetMuliBinIndex(nch);
        if (m_mult_i < 0 || m_mult_i >= Bins::NCENT) continue;

        ReconstructJet();

        h1_ntrk->Fill(nch);
    }
    std::cout << "Finish making global eta-phi plots" << std::endl;
    SaveHistos();

    return;
}

void ReconstructJet() {
    vector<PseudoJet> particles;
    int nch = final_p.size();
    for (int j = 0; j < nch; j++) {
        double pt  = final_p[j].pT ;
        double eta = final_p[j].eta;
        double phi = final_p[j].phi;
        double m   = final_p[j].m  ;

        TLorentzVector v;
        v.SetPtEtaPhiM(pt, eta, phi, m);

        particles.push_back(PseudoJet(v.Px(), v.Py(), v.Pz(), v.E()));
    }

    // choose a jet definition
    double R = 0.4;
    JetDefinition jet_def(antikt_algorithm, R);

    // run the clustering, extract the jets
    ClusterSequence cs(particles, jet_def);
    vector<PseudoJet> jets = sorted_by_pt(cs.inclusive_jets());

    for (unsigned i = 0; i < jets.size(); i++) {
        vector<PseudoJet> constituents = jets[i].constituents();
        double jetpt  = jets[i].pt();
        double jeteta = jets[i].eta();
        double jetphi = ConvertPhi(jets[i].phi());
        if (jetpt > 6.0) {
            h1_jetpt->Fill(jetpt);
            h2_jetEtaPhi->Fill(jeteta, jetphi);

            if      (jetpt <  10) h3_EtaPhi_jetNch[0]->Fill(jeteta, jetphi, constituents.size());
            else if (jetpt <  20) h3_EtaPhi_jetNch[1]->Fill(jeteta, jetphi, constituents.size());
            else if (jetpt <  30) h3_EtaPhi_jetNch[2]->Fill(jeteta, jetphi, constituents.size());
            else if (jetpt <  40) h3_EtaPhi_jetNch[3]->Fill(jeteta, jetphi, constituents.size());
            else if (jetpt <  50) h3_EtaPhi_jetNch[4]->Fill(jeteta, jetphi, constituents.size());
            else if (jetpt <  60) h3_EtaPhi_jetNch[5]->Fill(jeteta, jetphi, constituents.size());
            else if (jetpt <  70) h3_EtaPhi_jetNch[6]->Fill(jeteta, jetphi, constituents.size());
            else if (jetpt <  80) h3_EtaPhi_jetNch[7]->Fill(jeteta, jetphi, constituents.size());
            else if (jetpt <  90) h3_EtaPhi_jetNch[8]->Fill(jeteta, jetphi, constituents.size());
            else                  h3_EtaPhi_jetNch[9]->Fill(jeteta, jetphi, constituents.size());
        }
    }
}

int GetEventNch() {
    int eventSize = (*particle_id).size();
    int ntrk = 0;
    for (int j = 0; j < eventSize; j++) {
        int partid = (*particle_id)[j];
        if (abs(partid) != 211 && abs(partid) != 321 && abs(partid) != 2212) continue;
        double pT  = (*particle_pt )[j];
        double eta = (*particle_eta)[j];
        double phi = (*particle_phi)[j]; phi = ConvertPhi(phi);
        double tau = (*particle_tau)[j];
        tau *= 1E9 / TMath::C();
        if (fabs(eta) > 2.5) continue;
        if (       pT < 0.4) continue;
        h1_tau->Fill(tau);
        if (tau > 0 && tau < 300) continue;

        particle p;
        p.pT  = pT ;
        p.eta = eta;
        p.phi = phi;

        final_p.push_back(p);
        ntrk++;
    }
    return ntrk;
}


//nch based binning
int GetMuliBinIndex(float nch) {
    for (int i = 0; i < Bins::NCENT; i++) {
        if (nch >= (Bins::NTRACK_PPB_LO[i] - .0001)) {
            return i;
        }
    }
    return -1;
}


double ConvertPhi(double phi) {
    if (phi >  TMath::Pi()) phi = phi - 2 * TMath::Pi();
    if (phi < -TMath::Pi()) phi = phi + 2 * TMath::Pi();
    return phi;
}


void InitTree() {
    char name[600];
    fChain = new TChain("PyTree");
    if (inFolder) sprintf(name, "01RootFiles/%s/*.root", filePrefix.c_str());
    else          sprintf(name, "01RootFiles/%s.root"  , filePrefix.c_str());
    fChain->Add(name);
    fChain->SetBranchAddress("particle_id" , &particle_id );
    fChain->SetBranchAddress("particle_pt" , &particle_pt );
    fChain->SetBranchAddress("particle_eta", &particle_eta);
    fChain->SetBranchAddress("particle_phi", &particle_phi);
    fChain->SetBranchAddress("particle_m"  , &particle_m  );
    fChain->SetBranchAddress("particle_tau", &particle_tau);
}

void InitHistos() {
    char histname[600], histtitle[600];

    /*-----------------------------------------------------------------------------
     *  Create the output TFile
     *-----------------------------------------------------------------------------*/
    char name[600] = "";
    sprintf(name, "03GlobalEtaPhi/%s_globalEtaPhi.root", filePrefix.c_str());
    tmpf = new TFile(name, "recreate");

    h1_ntrk  = new TH1D("h1_ntrk" , "h1_ntrk;N_{ch};N",  200, -0.5, 199.5);
    h1_tau   = new TH1D("h1_tau"  , "h1_tau;#tau;N"   , 5000,  0  , 5000 );
    h1_jetpt = new TH1D("h1_jetpt", "h1_jetpt;p_{T};N", 2000,  0  ,  200 );
    h2_jetEtaPhi = new TH2D("h2_jetEtaPhi", "h2_jetEtaPhi;#eta;#phi;N", 50, -2.5, 2.5, 64, -acos(-1.0), acos(-1.0));

    /*-----------------------------------------------------------------------------
     *  3D Eta-phi map for convolution study
     *
     *-----------------------------------------------------------------------------*/
    for (int i = 0; i < 10; i++) {
        sprintf(histname , "h3_EtaPhi_jetNch_jetPt%d_%d", i * 10, i * 10 + 10);
        sprintf(histtitle, "h3_EtaPhi_jetNch;#eta;#phi;N_{ch}");
        h3_EtaPhi_jetNch[i] = new TH3D(histname, histtitle, EtaBins3D, -2.5, 2.5, PhiBins3D, -acos(-1.0), acos(-1.0), 50, 0.0, 50.0);
    }
}

void SaveHistos() {
    tmpf->cd();
    tmpf->Write();
    std::cout << "Saving Finished" << std::endl;
    delete tmpf;
    tmpf = 0;
}

