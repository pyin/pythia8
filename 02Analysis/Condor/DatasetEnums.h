#ifndef __DatasetEnums_h__
#define __DatasetEnums_h__
namespace DataSetEnums {
//-------------------------------------------------------------------------------------
enum {
    DATA_LOWMU                   = 0,
    DATA_LOW_AND_INTERMEDIATE_MU = 1,
    DATA_5TEV                    = 2,
    DATA_pPb_2016_8TeV           = 3,
    DATA_pp_REPROCESSED          = 4,

    DEFAULT_TYPE = DATA_LOW_AND_INTERMEDIATE_MU,
};

// -----------------------------------------
// p+p  @ 13 TeV: Data_LowMu,
//                Data_LowAndIntermediateMu,
// p+p  @  5 TeV: Data_5TeV,
// p+Pb @  8 TeV: Data_pPb_2016_8TeV2
// -----------------------------------------
std::map<int, std::string> Filename = {
    {DATA_LOWMU                  , "Data_LowMu"  } ,
    {DATA_LOW_AND_INTERMEDIATE_MU, "Data_LowAndIntermediateMu"  } ,
    {DATA_5TEV                   , "Data_5TeV"   } ,
    {DATA_pPb_2016_8TeV          , "Data_pPb_2016_8TeV2"  } ,
    {DATA_pp_REPROCESSED         , "Data_pp_REPROCESSED"},
};


std::map<int, std::string> Filename_eff = {
    {DATA_LOWMU                  , "02EfficiencyFiles/tracking_efficiencies_20150709.root"   } ,
    {DATA_LOW_AND_INTERMEDIATE_MU, "02EfficiencyFiles/tracking_efficiencies_20150709.root"   } ,
    {DATA_5TEV                   , "02EfficiencyFiles/tracking_efficiencies_20150709.root"   } ,
    {DATA_pPb_2016_8TeV          , "02EfficiencyFiles/tracking_efficiencies_20150709.root"   } ,
    {DATA_pp_REPROCESSED         , "02EfficiencyFiles/tracking_efficiencies_20150709.root"   } ,
};


std::map<int, std::string> Histname_eff = {
    {DATA_LOWMU                  , "Primary_efficiency_PtEta_A2SP"} ,
    {DATA_LOW_AND_INTERMEDIATE_MU, "Primary_efficiency_PtEta_A2SP"} ,
    {DATA_5TEV                   , "Primary_efficiency_PtEta_A2SP"} ,
    {DATA_pPb_2016_8TeV          , "Primary_efficiency_PtEta_A2SP"} ,
    {DATA_pp_REPROCESSED         , "Primary_efficiency_PtEta_A2SP"} ,
};


std::map<int, std::string> DATASETENERGY = {
    {DATA_LOWMU                  , "#sqrt{#it{s}}=13 TeV"}, //, L_{Tot}#approx14 nb^{-1}"} };
    {DATA_LOW_AND_INTERMEDIATE_MU, "#sqrt{#it{s}}=13 TeV, 64 nb^{-1}"},
    {DATA_5TEV                   , "#sqrt{#it{s}}=5.02 TeV" } ,
    {DATA_pPb_2016_8TeV          , "#sqrt{#it{s}_{NN}}=8.16 TeV, 171 nb^{-1}"} ,
    {DATA_pp_REPROCESSED         , "#sqrt{#it{s}}=13 TeV, 64 nb^{-1}"} ,
};

std::map<int, std::string> DATASETENERGY2 = {
    {DATA_5TEV                   , "#sqrt{#it{s}}=5.02 TeV" } ,
    {DATA_LOWMU                  , "#sqrt{#it{s}}=13 TeV"}, //, L_{Tot}#approx14 nb^{-1}"} };
    {DATA_LOW_AND_INTERMEDIATE_MU, "#sqrt{#it{s}}=13 TeV, 64 nb^{-1}"},
    {DATA_pPb_2016_8TeV          , "#sqrt{#it{s}_{NN}}=8.16 TeV"} ,
    {DATA_pp_REPROCESSED         , "#sqrt{#it{s}}=13 TeV, 64 nb^{-1}"},
};

std::map<int, std::string> DATASETLABEL = {
    {DATA_LOWMU                  , "#it{pp}"} ,
    {DATA_LOW_AND_INTERMEDIATE_MU, "#it{pp}"} ,
    {DATA_5TEV                   , "#it{pp}"} ,
    {DATA_pPb_2016_8TeV          , "#it{p}+Pb"} ,
    {DATA_pp_REPROCESSED         , "#it{pp}"} ,
};
//-------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------
enum {
    NO_MU_LIMIT = 0, // for old Low mu and inter mu data set
    MU_0TO1     = 1, // mu: 0 - 1
    MU_1TO2     = 2, // mu: 1 - 2
    MU_2TO3     = 3, // mu: 2 - 3
};

std::map<int, std::string> MURANGE = {
    {NO_MU_LIMIT, "No limit (old set)"},
    {MU_0TO1    , "0 < #mu #leq 1"},
    {MU_1TO2    , "1 < #mu #leq 2"},
    {MU_2TO3    , "2 < #mu #leq 3"}
};
//-------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------
enum {
    NO_EFFICIENCY      = 0, //No eff whatsoever
    DEFAULT_EFFICIENCY = 1, //Efficiency for PTY only
    SYSERRO_EFFICIENCY = 2, //Efficiency for PTY only
};

std::map<int, std::string> EFFLABEL = {
    {NO_EFFICIENCY     , "No Eff Correction"},
    {DEFAULT_EFFICIENCY, "Eff Corrected"},
    {SYSERRO_EFFICIENCY, "SysError eff"}
};
//-------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------
enum {
    ALL_TRIGS        = 0,
    MINBIAS_ONLY     = 1,
    NTRK_CUT_FOR_HMT = 2,
};
std::map<int, std::string> TRIGLABEL = {
    {ALL_TRIGS       , "All Triggers"},
    {MINBIAS_ONLY    , "Min Bias"    },
    {NTRK_CUT_FOR_HMT, "Ntrk Cut"    },
};
//-------------------------------------------------------------------------------------



//-------------------------------------------------------------------------------------
enum {
    NO_PILEUP_REJECTION                  = 0,
    REJECTION_NTRK5_ZVTXLT15             = 1,
    REJECTION_NTRK10_ZVTXLT15            = 2,
    REJECTION_SUMPT5_ZVTXLT15            = 3,
    REJECTION_SUMPT10_ZVTXLT15           = 4,
    REJECTION_REMOVE_2VTX_EVENTS         = 5,
    REJECTION_REMOVE_CLOSE15_2VTX_EVENTS = 6,
    REJECTION_REMOVE_CLOSE10_2VTX_EVENTS = 7,
    REJECTION_REMOVE_CLOSE5_2VTX_EVENTS  = 8,
};
//-------------------------------------------------------------------------------------


// -------------------------------------------------------------------------------------
// ANTIKT2: R = 0.2  |
// ANTIKT3: R = 0.3  |
// ANTIKT4: R = 0.4  |
// -------------------
enum {
    NO_JET_REJECT = 0,
    ANTIKT2       = 1,
    ANTIKT3       = 2,
    ANTIKT4       = 3,
};
std::map<int, std::string> JETLABEL = {
    {NO_JET_REJECT, "No Jet rejection"  },
    {ANTIKT2      , "AntiKt2 Track Jets"},
    {ANTIKT3      , "AntiKt3 Track Jets"},
    {ANTIKT4      , "AntiKt4 Track Jets"},
};
// -------------------------------------------------------------------------------------


// -------------------------------------------------------------------------------------
enum {
    NO_JET_CUTS          = 0,
    DETA4_MINPT6         = 1,
    DETA4_MINPT6_CONST2  = 2,

    DETA1_MINPT15_CONST2 = 3,
    DETA2_MINPT15_CONST2 = 4,
    DETA3_MINPT15_CONST2 = 5,
    DETA4_MINPT15_CONST2 = 6,

    DETA4_MINPT15_SNDPT9_CONST2  = 7,
    DETA4_MINPT15_SNDPT12_CONST2 = 8,
    DETA4_MINPT15_SNDPT15_CONST2 = 9,

    DETA4_MINPT15_SNDPT100_CONST2 = 10,
    DETA4_MINPT15_SNDPT6_CONST2   = 11,
    DETA4_MINPT15_SNDPT7_CONST2   = 12,
    DETA4_MINPT15_SNDPT8_CONST2   = 13,
};
//-------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------
enum {
    NOJET   = 0,
    WITHJET = 1,
    MINBIAS = 2,
};
//-------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------
enum {
    NO_UECORR   = 0,
    WITH_UECORR = 1,
};
//-------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------
enum {
    HADRON_HADRON_CORRELATIONS = 0,
    HADRON_JET_CORRELATIONS    = 1,
    HADRON_NOJET_CORRELATIONS  = 2,
};
//-------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------
enum {
    USE_NTRK    = 0,
    USE_FCAL_ET = 1,
};
//-------------------------------------------------------------------------------------



#define DEFAULT_SETTINGS \
  int l_data_type         = DataSetEnums::DEFAULT_TYPE, \
  int l_mu_limit          = DataSetEnums::NO_MU_LIMIT,\
  int l_apply_efficiency  = DataSetEnums::DEFAULT_EFFICIENCY,\
  int l_trig_type         = DataSetEnums::NTRK_CUT_FOR_HMT,\
  int l_pileup_reject     = DataSetEnums::NO_PILEUP_REJECTION, \
  int l_jet_reject_type   = DataSetEnums::NO_JET_REJECT, \
  int l_jet_reject_cuts   = DataSetEnums::NO_JET_CUTS, \
  int l_if_have_jet       = DataSetEnums::MINBIAS, \
  int l_if_have_uecorr    = DataSetEnums::WITH_UECORR, \
  int l_correlation_type  = DataSetEnums::HADRON_HADRON_CORRELATIONS, \
  int l_multiplicity_type = DataSetEnums::USE_NTRK \

#define SET_DEFAULT_SETTINGS \
  int l_data_type         = DataSetEnums::DEFAULT_TYPE;      \
  int l_mu_limit          = DataSetEnums::NO_MU_LIMIT,\
  int l_apply_efficiency  = DataSetEnums::DEFAULT_EFFICIENCY;\
  int l_trig_type         = DataSetEnums::NTRK_CUT_FOR_HMT;\
  int l_pileup_reject     = DataSetEnums::NO_PILEUP_REJECTION; \
  int l_jet_reject_type   = DataSetEnums::NO_JET_REJECT; \
  int l_jet_reject_cuts   = DataSetEnums::NO_JET_CUTS; \
  int l_if_have_jet       = DataSetEnums::MINBIAS, \
  int l_if_have_uecorr    = DataSetEnums::WITH_UECORR, \
  int l_correlation_type  = DataSetEnums::HADRON_HADRON_CORRELATIONS; \
  int l_multiplicity_type = DataSetEnums::USE_NTRK; \

#define PASS_SETTINGS \
  l_data_type       ,l_mu_limit         ,l_apply_efficiency ,\
  l_trig_type       ,l_pileup_reject    ,\
  l_jet_reject_type ,l_jet_reject_cuts  ,\
  l_if_have_jet     ,l_if_have_uecorr   ,\
  l_correlation_type,l_multiplicity_type

#define PASS_SETTINGS2 \
  m_data_type       ,m_mu_limit         ,m_apply_efficiency ,\
  m_trig_type       ,m_pileup_reject    ,\
  m_jet_reject_type ,m_jet_reject_cuts  ,\
  m_if_have_jet     ,m_if_have_uecorr   ,\
  m_correlation_type,m_multiplicity_type


#define PASS_SETTINGS3 0,0,0,0,0,0,0,0,0,0,0

#define ACCEPT_SETTINGS\
  int l_data_type       , int l_mu_limit         , int l_apply_efficiency ,\
  int l_trig_type       , int l_pileup_reject    ,\
  int l_jet_reject_type , int l_jet_reject_cuts  ,\
  int l_if_have_jet     , int l_if_have_uecorr   ,\
  int l_correlation_type, int l_multiplicity_type


#define PRINT_SETTINGS\
  std::cout << l_data_type        << "," << l_mu_limit          << "," << l_apply_efficiency  << ","\
            << l_trig_type        << "," << l_pileup_reject     << ","\
            << l_jet_reject_type  << "," << l_jet_reject_cuts   << ","\
            << l_if_have_jet      << "," << l_if_have_uecorr    << ","\
            << l_correlation_type << "," << l_multiplicity_type << std::endl;


#define BASENAME  DataSetEnums::BaseName(PASS_SETTINGS)

std::string BaseName(ACCEPT_SETTINGS) {
    std::cout << "**************************************" << std::endl;
    PRINT_SETTINGS;
    std::cout << "**************************************" << std::endl;

    char name[600] = "";
    sprintf(name, "%s", Filename[l_data_type].c_str());
    std::string ret = name;
    if (l_mu_limit          != NO_MU_LIMIT               ) sprintf(name, "%s_mu%d", ret.c_str(), l_mu_limit);
    ret = name;
    sprintf(name, "%s_eff%d", ret.c_str(), l_apply_efficiency);
    ret = name;

    if (l_trig_type         != ALL_TRIGS                 ) sprintf(name, "%s_trig%d"  , ret.c_str(), l_trig_type);
    ret = name;
    if (l_pileup_reject     != NO_PILEUP_REJECTION       ) sprintf(name, "%s_pileup%d", ret.c_str(), l_pileup_reject);
    ret = name;
    if (l_jet_reject_type   != NO_JET_REJECT             ) sprintf(name, "%s_jet%d_jetcut%d_ifHaveJet%d_ifUECorr%d", ret.c_str(), l_jet_reject_type, l_jet_reject_cuts, l_if_have_jet, l_if_have_uecorr);
    ret = name;
    if (l_correlation_type  != HADRON_HADRON_CORRELATIONS) sprintf(name, "%s_corrtype%d", ret.c_str(), l_correlation_type);
    ret = name;
    sprintf(name, "%s_multtype%d"    , ret.c_str(), l_multiplicity_type);
    ret = name;
    return ret;
}


void CheckConfigurables(ACCEPT_SETTINGS) {
    bool pass = true;
    if (pass == false)  throw std::exception();
}


};//namespace DataSetEnums
#endif
