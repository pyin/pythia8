#ifndef _AnaCorrPythia8_HH_
#define _AnaCorrPythia8_HH_

#include <vector>
#include "Riostream.h"
#include "TChain.h"
#include <string>
#include <Event.h>

#include "map"
#include "DatasetEnums.h"
#include "bins.h"
#include "TH3.h"
#include <TProfile.h>
#include <TProfile2D.h>


using namespace std;

class TH1;
class TH2;
class TProfile;

struct triple {
    std::string first;
    Bool_t*     second;
    int         third;
};

class AnaCorrPythia8 {
public:
    AnaCorrPythia8();

    void run();

    int m_data_type         = 1;
    int m_mu_limit          = 0;
    int m_apply_efficiency  = 1;
    int m_trig_type         = 2;
    int m_pileup_reject     = 8;
    int m_jet_reject_type   = 3;
    int m_jet_reject_cuts   = 6;
    int m_if_have_jet       = 2;
    int m_if_have_uecorr    = 1;
    int m_correlation_type  = 2;
    int m_multiplicity_type = 0;
    string filePrefix;// = "pytree_MONASH13_HardQCD_10M";
    bool inFolder;

private:
    TChain *fChain = nullptr;


    void InitTree();
    void InitTriggers();
    void InitHistos();
    void SaveHistos();
    void InitJetCuts();
    void ReconstructJet();
    void InitUEpT();

    void parseJets();
    void ProcessJetEnergyFraction();
    void Fill(Event* event1, Event* event2, int mixtype);
    void FillJetMonitors();
    void FillOriginalJetMonitors();
    void ResetJetIngredientArray();

    bool RejectJet_Cone(float trk_pt_, float trk_eta_, float trk_phi_, float trk_eff_);

    int GetNtrkCent();
    int GetCentBinNtrk(float ntrk);
    std::pair<int, float> CountLowPTParticle(int ijet, float jet_eta);

    double MultiplicityCorrection();
    double ConvertPhi(double phi);
    double CalculateDeltaPhi(float phi1, float phi2);
    double EvaluateTrackingEff(float pt, float eta);
    double findUECorr(TProfile2D* h, double jet_eta, double jet_phi);
    double interpolate(double low, double up, double val_low, double val_up, double x);


    // ------------------------------------
    // Jet cut variables
    //
    float dR = -1;
    float min_jet_pt     = 1e10;
    float min_jet_pt_2nd = 1e10;
    int   min_jet_Nconstituents = 0; //Minimum Number of constituents
    // ------------------------------------

    int Ncurr = 0;
    int m_cent_i;
    int o_cent_i;
    int ntrk_cent;

    double m_zvtx = 0;
    double ntrk_correction;
    double jetIngredientPT [200][200];
    double jetIngredientETA[200][200];
    double jetIngredientPHI[200][200];

    vector<int> used_Partical;

    vector<double> m_jet_Nconstituents;
    vector<double> evt_jet_eta;
    vector<double> evt_jet_phi;

    vector<float>  reco_jet_pt ;
    vector<float>  reco_jet_eta;
    vector<float>  reco_jet_phi;
    vector<float>  reco_jet_Nch;

    vector<bool> m_jet_pass1;    // leading jet : eg. 15GeV
    vector<bool> m_jet_pass2;    // sub-leading jet : eg. 10GeV

    TH1* hcent;
    TH1* hcent_orig;
    TH1* hNtrk_origional;
    TH1* hNtrk_corrected;
    TH1* h_MultCorrection;
    TH1* h_JetEta2nd[Bins::NCENT];
    TH1* dist_pT_original    [Bins::NCENT];
    TH1* dist_pT_corred      [Bins::NCENT];
    TH1* dist_pT_afterRej_jet[Bins::NCENT];
    TH1* dist_pT_afterRej_ref[Bins::NCENT];
    TH1* h_jet_numJets  [Bins::NCENT];      // distribution of "# of 15 GeV jets in an event" in each centrality bin
    TH1* h_jet_counts;                      //# of 15 GeV jets in each centrality bin
    TH1* N_trigger_jetcut [Bins::NCENT];    //pT spectra of tracks within the jet cone.
    TH1* h_bg_constituents[Bins::NCENT];    // value of constituents correction
    TH1* h_bg_ptWeight    [Bins::NCENT];    // value of pT correction
    TH1* h_UE_pTThreshold [Bins::NCENT];    // value of pT correction
    // -----------------------------------
    // global monitor plots
    //    record how many event processed
    TH1* hFillEntries;
    TH1* hFillMixEntries;
    // -----------------------------------


    TH2* h_efficiency = nullptr;
    TH2* h2_Mult_CorrMult;
    TH2* h2_Mult_FCal;
    TH2* h2_CorrJetpT_JetpT[Bins::NCENT];   // corrected jet pT  vs original jet pT
    TH2* h2_JetpTCorr_JetpT[Bins::NCENT];   // jet pT correction vs original jet pT
    TH2* h2_CorrJetNch_JetNch[Bins::NCENT]; // corrected jet Nch  vs original jet Nch
    TH2* h2_JetNchCorr_JetNch[Bins::NCENT]; // jet Nch correction vs original jet Nch
    TH2* h2_jetPT_jetNch      [Bins::NCENT];
    TH2* h2_jetPT_jetNch_orig [Bins::NCENT];
    TH2* h2_jetPT_jetNch_ocent[Bins::NCENT];
    TH2* fg[Bins::NCENT][Bins::NPT1][Bins::NPT2][Bins::NCH];
    TH2* h_10Jet_15Jet [Bins::NCENT];                   // N_10GeVJet vs N_15GeVJet
    TH2* h_jet_dEtadPhi_LnSL_sameCut[Bins::NCENT];      // deta:dphi distribution of leading and subleading jets
    TH2* h_jet_eta_LnSL_sameCut[Bins::NCENT];           // eta of leading jet vs subleading jet
    TH2* h_jet_phi_LnSL_sameCut[Bins::NCENT];           // phi of leading jet vs subleading jet
    TH2* h_jet_pT_LnSL_sameCut [Bins::NCENT];           // pT of leading jet vs subleading jet
    TH2* h_jet_dEtadPhi_LnSL_1st15[Bins::NCENT];        // deta:dphi distribution of leading and subleading jets
    TH2* h_jet_eta_LnSL_1st15[Bins::NCENT];             // eta of leading jet vs subleading jet
    TH2* h_jet_phi_LnSL_1st15[Bins::NCENT];             // phi of leading jet vs subleading jet
    TH2* h_jet_pT_LnSL_1st15 [Bins::NCENT];             // pT of leading jet vs subleading jet

    TH3* h_EtaPhiPt[Bins::NCENT];

    TProfile* p_Ntrk;
    TProfile* p_MultBins_withCorrMult;
    TProfile* p_CorrMultBins_withMult;
    TProfile* p_v2[Bins::NCENT];
    TProfile* p_v2_jetcut[Bins::NCENT];
    TProfile* p_jetdNch_noOSCorr[Bins::NCENT];
    TProfile* p_jetdpT_noOSCorr [Bins::NCENT];
    TProfile* p_jetdNch[Bins::NCENT];
    TProfile* p_jetdpT [Bins::NCENT];

    TProfile2D* h_EtaPhi_global         [Bins::NCENT];
    TProfile2D* h_EtaPhi_ptWeight_global[Bins::NCENT];

    vector<int>    *particle_id  = 0;
    vector<double> *particle_pt  = 0;
    vector<double> *particle_eta = 0;
    vector<double> *particle_phi = 0;
    vector<double> *particle_m   = 0;
    vector<double> *particle_tau = 0;
};

void AnaCorrPythia8::InitTree() {
    fChain = new TChain("PyTree");

    char name[600];
    // sprintf(name, "01RootFiles/%s.root", filePrefix.c_str());
    sprintf(name, "01RootFiles/%s/*.root", filePrefix.c_str());
    fChain->Add(name);
    fChain->SetBranchAddress("particle_id" , &particle_id );
    fChain->SetBranchAddress("particle_pt" , &particle_pt );
    fChain->SetBranchAddress("particle_eta", &particle_eta);
    fChain->SetBranchAddress("particle_phi", &particle_phi);
    fChain->SetBranchAddress("particle_m"  , &particle_m  );
    fChain->SetBranchAddress("particle_tau", &particle_tau);
}
#endif
