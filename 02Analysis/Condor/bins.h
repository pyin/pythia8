#ifndef __BINS_H__
#define __BINS_H__

#include <string>
#include <map>
#include <utility>
#include <Riostream.h>

#include "DatasetEnums.h"
#include "common.C" //double PI=acos(-1.0);
// #include "Defs.h"

namespace Bins {
std::string HH_LABEL  = "#it{h}-#it{h} Correlations";
std::string HJ_LABEL  = "#it{h}-#it{h(jet)} Correlations";
std::string HNJ_LABEL = "#it{h}-#it{h} Correlations";

int  Initialize();
void Initialize_CentAdd();
void Initialize_Pt1Add();
void Initialize_Pt2Add();


std::string label_cent       (int icent);
std::string label_FCalEt     (int icent);
std::string label_cent_short (int icent);
std::string label_cent_peri  (int icent);
std::string label_FCalEt_peri(int icent);
int GetCentIndex              (int mult_low, int mult_high);
std::vector<int> GetCentIndex (std::vector<std::pair<int, int>> input);
std::vector<int> GetCentIndex (std::vector<double> input);
std::pair<double, double> GetCentVals(int icent);


std::string label_pta(int ipt, int correlation_type = DataSetEnums::HADRON_HADRON_CORRELATIONS);
int GetPtaIndex(double pta_low, double pta_high);
std::vector<int> GetPtaIndex (std::vector<std::pair<double, double>> input);
std::vector<int> GetPtaIndex (std::vector<double> input);


std::string label_ptb(int ipt, int correlation_type = DataSetEnums::HADRON_HADRON_CORRELATIONS);
std::string label_ptb2(int ipt, int correlation_type = DataSetEnums::HADRON_HADRON_CORRELATIONS);
int GetPtbIndex(double ptb_low, double ptb_high);
std::vector<int> GetPtbIndex (std::vector<std::pair<double, double>> input);
std::vector<int> GetPtbIndex (std::vector<double> input);
std::pair<double, double> GetPtbVals(int ipt);


std::string label_ptab(int ipta, int iptb, int correlation_type = DataSetEnums::HADRON_HADRON_CORRELATIONS);
int GetPtbIndexForPtaIndex(int ipta);    //obsolete causes problems
//int GetPtaIndexForPtbIndex(int iptb);


std::string label_eta(int ideta);
int GetDetaIndex(double deta_low, double deta_high);
std::vector<int> GetDetaIndex (std::vector<std::pair<double, double>> input);


const int NO_PERIPHERAL_BIN = -1;
std::pair<float, float> GetVnn  (int icent, int ipt1, int ipt2, int ich, int ideta, int ihar, int icent_periph, TFile *FourierFile);
std::pair<float, float> GetVnPtb(int icent, int ipt1, int ipt2, int ich, int ideta, int ihar, int icent_periph, TFile *FourierFile1, TFile *FourierFile2);


int GetPtBin1(float pt);
int GetPtBin2(float pt);


TH1D* CentdepHist(std::vector<int>centbins, int l_centrality_type, std::string name) ;
TH1D* PtadepHist(std::vector<int>ipt1_vec, std::string name);
TH1D* PtbdepHist(std::vector<int>ipt2_vec, std::string name, int correlation_type);




std::vector<int> CentBins(ACCEPT_SETTINGS);
std::vector<int> CentBinsPeriph(ACCEPT_SETTINGS);
std::vector<int> PtaBins (ACCEPT_SETTINGS);
std::vector<int> PtbBins (ACCEPT_SETTINGS);
std::vector<int> DetaBins(ACCEPT_SETTINGS);
std::vector<int> ChBins  (ACCEPT_SETTINGS);




enum {
  BINS_SUMMARY       = 6,
  BIN_FUNC_INTEGRAL  = 1,
  BIN_HIST_INTEGRAL  = 2,
  BIN_FUNC_CHI2_NDOF = 3,
  BIN_ZYAM_MIN_ERR   = 4,
  BIN_ZYAM_ERR_HIST  = 5,
  BIN_ZYAM_ERR_FUNC  = 6,
  BIN_ZYAM_MIN_VAL   = 7,
};
std::map<int, std::string> BINS_SUMMARY_LABELS = {
  {BIN_ZYAM_MIN_ERR  , "Error of ZYAM Min"        },
  {BIN_ZYAM_MIN_VAL  , "Pedestal of ZYAM"         },
  {BIN_FUNC_INTEGRAL , "PTY Near Side (from Fit)" },
  {BIN_HIST_INTEGRAL , "PTY Near Side (from Hist)"},
  {BIN_FUNC_CHI2_NDOF, "CHI2/NDOF of Fit"         },
  {BIN_ZYAM_ERR_HIST , "ZYAM Error (hist)"        },
  {BIN_ZYAM_ERR_FUNC , "ZYAM Error (function)"    },
};

enum {
  VN_TYPE                         = 7,
  VN_TEMPLATE                     = 0, //ZYAM
  VN_TEMPLATE_PEDESTAL            = 1, //No ZYAM
  VN_DIRECT                       = 2,
  VN_PERISUB                      = 3,
  VN_TEMPLATE_PEDESTAL_HJHHperiph = 4,
  VN_TEMPLATE_HJHHperiph          = 5,
  VN_TEMPLATE_PEDESTALMINBIAS_HJHHperiph = 6,
};
std::string label_vn_type(int vn_type) {
  if      (vn_type == VN_TEMPLATE                    ) return "Template Fits (ZYAM)"  ;
  else if (vn_type == VN_TEMPLATE_PEDESTAL           ) return "Template Fits"         ;
  else if (vn_type == VN_DIRECT                      ) return "Fourier Transform"     ;
  else if (vn_type == VN_PERISUB                     ) return "Peripheral Subtraction";
  else if (vn_type == VN_TEMPLATE_PEDESTAL_HJHHperiph) return "Template Fits";
  else if (vn_type == VN_TEMPLATE_HJHHperiph)          return "Template Fits (ZYAM)";
  else if (vn_type == VN_TEMPLATE_PEDESTALMINBIAS_HJHHperiph)          return "Template Fits (jet ZYAM)";
  else {
    std::cout << __PRETTY_FUNCTION__ << ":: Unknown vn type" << std::endl;
    throw std::exception();
  }
}
//-------------------------------------------------------------------------------------
//only used in analysis, not in production
enum {
  REMOVE_PERIPHERAL_PEDESTAL_OldMethod = 0,
  KEEP_PERIPHERAL_PEDESTAL_NewMethod  = 1,
};
//-------------------------------------------------------------------------------------



enum {
  NCENT = 40,
  NPT1  = 6,
  NPT2  = 37,
  NCH   = 2,

  SAME_CHARGE     = 0,
  OPPOSITE_CHARGE = 1,

  NCENT_ADD = 24,

  NPT1_ADD        = 5,
  NPT2_ADD        = 17,
  NCH_ADD         = 1,
  COMBINED_CHARGE = 2,
  NETA            = 11,
};

enum {
  NHAR_DFT = 4,
  v1 = 1,
  v2 = 2,
  v3 = 3,
  v4 = 4,
};




char label[600];

//---------------------------------------------------------------------------------------------------------------------
int NTRACK_PPB_LO[NCENT + NCENT_ADD]
= {
//  0    1    2    3    4    5    6    7    8    9   10   11   12   13   14   15
  480, 460, 440, 420, 400, 380, 360, 340, 320, 300, 280, 260, 240, 220, 200, 180,
// 16   17   18   19   20   21   22   23   24   25   26   27
  170, 160, 150, 140, 130, 120, 110, 100,  90,  80,  70,  60,
//28   29   30   31   32   33   34   35   36   37   38   39
  55,  50,  45,  40,  35,  30,  25,  20,  15,  10,   5,   0,
};
int NTRACK_PPB_HI[NCENT + NCENT_ADD]
= {
//  0    1    2    3    4    5    6    7    8    9   10   11   12   13   14   15
  500, 480, 460, 440, 420, 400, 380, 360, 340, 320, 300, 280, 260, 240, 220, 200,
// 16   17   18   19   20   21   22   23   24   25   26   27
  180, 170, 160, 150, 140, 130, 120, 110, 100,  90,  80,  70,
//28   29   30   31   32   33   34   35   36   37   38   39
  60,  55,  50,  45,  40,  35,  30,  25,  20,  15,  10,   5,
};

int cent_add_lo[NCENT_ADD] = {0};
int cent_add_up[NCENT_ADD] = {0};
void Initialize_CentAdd() {
  int *CENT_HI = NTRACK_PPB_HI;
  int *CENT_LO = NTRACK_PPB_LO;
  std::vector<std::pair<double, double>> new_cent_bins = {
  //      40        41        42        43         44          45
    { 0,  10}, {10, 20}, {20, 30}, {30, 40}, {40,  50}, { 50,  60},
  //      46        47        48        49         50          51          52          53          54
    { 0,  20}, {20, 40}, {40, 60}, {60, 80}, {80, 100}, {100, 120}, {120, 140}, {140, 160}, {160, 180},
  //      55        56        57        58         59
    {10,  30}, {10, 40}, { 0, 30}, {50, 70}, { 5,  15},
  //      60         61          62         63
    {20,  60}, {60, 100}, {100, 140}, {60, 150}
  };

  if (new_cent_bins.size() != NCENT_ADD) {
    std::cout << __PRETTY_FUNCTION__ << "::new_cent_bins.size()!=CENT_ADD " << new_cent_bins.size() << "  " << NCENT_ADD << std::endl;
    throw std::exception();
  }

  int ibin = 0;
  for (auto new_bin : new_cent_bins) {
    int low = -1, high = -1;
    for (int icent = 0; icent < NCENT; icent++) {
      if (fabs(CENT_LO[icent] - new_bin.first ) < 0.001 ) high = icent + 1; //NOTE high and low are flipped because 0=highest centrality
      if (fabs(CENT_HI[icent] - new_bin.second) < 0.001 ) low = icent;
    }
    if (low == -1 || high == -1 || low >= high) {
      std::cout << __PRETTY_FUNCTION__ << "::Problem adding new bin" << std::endl;
      throw std::exception();
    }

    cent_add_lo[ibin] = low;
    cent_add_up[ibin] = high;
    CENT_LO[NCENT + ibin] = new_bin.first;
    CENT_HI[NCENT + ibin] = new_bin.second;
    //std::cout<<__PRETTY_FUNCTION__<<" "<<NCENT+ibin<<"  "<<CENT_LO[NCENT+ibin]<<"  "
    //         <<CENT_HI[NCENT+ibin]<<"  "<<cent_add_lo[ibin]<<"  "<<cent_add_up[ibin]<<std::endl;
    ibin++;
  }

  //checks
  //make sure edge of CENT_LO matches edge of previous CENT_HI for original NCENT bins
  for (int icent = 1; icent < NCENT; icent++) {
    if (CENT_LO[icent - 1] != CENT_HI[icent]) {
      std::cout << __PRETTY_FUNCTION__ << "Error in (CENT_LO,CENT_HI) original at index " << icent << std::endl;
      throw std::exception();
    }
  }
  //make sure that no duplicate Centrality bins are produced
  for (int icent = NCENT; icent < NCENT + NCENT_ADD; icent++) {
    if (GetCentIndex(CENT_LO[icent], CENT_HI[icent]) != icent) {
      std::cout << __PRETTY_FUNCTION__ << " Duplicate CENT bin found at index " << icent << std::endl;
      throw std::exception();
    }
  }
}

std::string label_cent_short(int icent) {
  if (NTRACK_PPB_HI[icent] < 500) sprintf(label, "[%d,%d)", NTRACK_PPB_LO[icent], NTRACK_PPB_HI[icent]);
  else                            sprintf(label, "#geq%d" , NTRACK_PPB_LO[icent]);
  std::string ret = label;
  return ret;
}

std::string label_cent(int icent) {
  if (NTRACK_PPB_HI[icent] < 500) sprintf(label, "%d#leq#it{N}_{ch}^{rec,corr}<%d", NTRACK_PPB_LO[icent], NTRACK_PPB_HI[icent]);
  else                            sprintf(label, "#it{N}_{ ch}^{rec}#geq%d"   , NTRACK_PPB_LO[icent]);
  std::string ret = label;
  return ret;
}

std::string label_FCalEt(int icent) {
  if (NTRACK_PPB_HI[icent] < 500) sprintf(label, "%d#leq#it{E}_{T}^{FCal}<%d", NTRACK_PPB_LO[icent], NTRACK_PPB_HI[icent]);
  else                            sprintf(label, "#it{E}_{T}^{FCal}#geq%d"   , NTRACK_PPB_LO[icent]);
  std::string ret = label;
  return ret;
}

std::string label_cent_peri(int icent) {
  if (NTRACK_PPB_HI[icent] < 500) sprintf(label, "%d#leq#it{N}_{ch}^{periph}<%d", NTRACK_PPB_LO[icent], NTRACK_PPB_HI[icent]);
  else                            sprintf(label, "#it{N}_{ch}^{periph}#geq%d"   , NTRACK_PPB_LO[icent]);
  std::string ret = label;
  return ret;
}

std::string label_FCalEt_peri(int icent) {
  if (NTRACK_PPB_HI[icent] < 500) sprintf(label, "%d#leq#it{E}_{T}^{FCal periph}<%d", NTRACK_PPB_LO[icent], NTRACK_PPB_HI[icent]);
  else                            sprintf(label, "#it{E}_{T}^{FCal periph}#geq%d"   , NTRACK_PPB_LO[icent]);
  std::string ret = label;
  return ret;
}


int GetCentIndex(int mult_low, int mult_high) {
  for (int index = 0; index < NCENT + NCENT_ADD; index++) {
    if (NTRACK_PPB_LO[index] == mult_low && NTRACK_PPB_HI[index] == mult_high) return index;
  }
  std::cout << "Wrong centrality or cent_type called for GetCentIndex(): bin=("
            << mult_low << "," << mult_high << std::endl;
  throw std::exception();
}

std::vector<int> GetCentIndex (std::vector<std::pair<int, int>> input) {
  std::vector<int> ret;
  for (auto& mult_bin : input) {
    ret.push_back(GetCentIndex(mult_bin.first, mult_bin.second));
  }
  return ret;
}
std::vector<int> GetCentIndex (std::vector<double> input) {
  std::vector<int> ret;
  for (unsigned int i = 0; i < input.size() - 1; i++) {
    ret.push_back(GetCentIndex(input[i], input[i + 1]));
  }
  return ret;
}

std::pair<double, double> GetCentVals(int icent) {
  if (icent >= (NCENT + NCENT_ADD) || icent < 0) {
    std::cout << "This Centrality index doesnot exist :" << icent << std::endl;
    throw std::exception();
  }

  double lo, hi;
  lo = NTRACK_PPB_LO[icent];
  hi = NTRACK_PPB_HI[icent];
  return std::make_pair(lo, hi);
}
//---------------------------------------------------------------------------------------------------------------------







//---------------------------------------------------------------------------------------------------------------------
/*-----------------------------------------------------------------------------
 *  PTA binning and labelling functions
 *-----------------------------------------------------------------------------*/
//LABELS
float PT1_LO[NPT1 + NPT1_ADD] = {0.3, 0.4, 0.5, 1.0, 2.0, 3.0};
float PT1_HI[NPT1 + NPT1_ADD] = {0.4, 0.5, 1.0, 2.0, 3.0, 5.0};
//BINS
int pt1_add_lo[NPT1_ADD] = {0};
int pt1_add_up[NPT1_ADD] = {0};
void Initialize_Pt1Add() {
  std::vector<std::pair<double, double>> new_pt1_bins = {
    //  6          7            8            9          10
    {0.5, 5.0}, {1.0, 5.0}, {2.0, 5.0}, {0.4, 3.0}, {1.0, 3.0}
  };

  if (new_pt1_bins.size() != NPT1_ADD) {
    std::cout << "Initialize_Pt1Add()::new_pt1_bins.size()!=NPT1_ADD " << new_pt1_bins.size() << "  " << NPT1_ADD << std::endl;
    throw std::exception();
  }

  int ibin = 0;
  for (auto new_bin : new_pt1_bins) {
    int low = -1, high = -1;
    for (int ipt1 = 0; ipt1 < NPT1; ipt1++) {
      if (fabs(PT1_LO[ipt1] - new_bin.first ) < 0.001 ) low = ipt1;
      if (fabs(PT1_HI[ipt1] - new_bin.second) < 0.001 ) high = ipt1 + 1;
    }
    if (low == -1 || high == -1 || low >= high) {
      std::cout << "Initialize_Pt1Add():: Problem adding new bin" << std::endl;
      throw std::exception();
    }

    pt1_add_lo[ibin] = low;
    pt1_add_up[ibin] = high;
    PT1_LO[NPT1 + ibin] = new_bin.first;
    PT1_HI[NPT1 + ibin] = new_bin.second;
    ibin++;
  }

  //checks
  //make sure edge of PT1_LO matches edge of previous PT1_HI for original NPT1 bins
  for (int ipt1 = 1; ipt1 < NPT1; ipt1++) {
    if (PT1_LO[ipt1] != PT1_HI[ipt1 - 1]) {
      std::cout << "Error in (PT1_LO,PT1_HI) original at index " << ipt1 << std::endl;
      throw std::exception();
    }
  }
  //make sure that no duplicate pt1 bins are produced
  for (int ipt1 = NPT1; ipt1 < NPT1 + NPT1_ADD; ipt1++) {
    if (GetPtaIndex(PT1_LO[ipt1], PT1_HI[ipt1]) != ipt1) {
      std::cout << "Duplicate PT1 bin found at index " << ipt1 << std::endl;
      throw std::exception();
    }
  }
}
std::string label_pta(int ipt, int correlation_type) {
  std::string                                                  label1 = "#it{a}";
  if (correlation_type == DataSetEnums::HADRON_JET_CORRELATIONS) label1 = "#it{h}";

  sprintf(label, "%.1f<#it{p}_{T}^{%s}<%.1f GeV", PT1_LO[ipt], label1.c_str(), PT1_HI[ipt]);
  if (PT1_LO[ipt] == int(PT1_LO[ipt]))   sprintf(label, "%d<#it{p}_{T}^{%s}<%.1f GeV", int(PT1_LO[ipt]), label1.c_str(),   (PT1_HI[ipt]));
  if (PT1_HI[ipt] == int(PT1_HI[ipt]))   sprintf(label, "%.1f<#it{p}_{T}^{%s}<%d GeV",   (PT1_LO[ipt]), label1.c_str(), int(PT1_HI[ipt]));
  if (PT1_LO[ipt] == int(PT1_LO[ipt])
      && PT1_HI[ipt] == int(PT1_HI[ipt]))   sprintf(label, "%d<#it{p}_{T}^{%s}<%d GeV"  , int(PT1_LO[ipt]), label1.c_str(), int(PT1_HI[ipt]));

  std::string ret = label;
  return ret;
}
int GetPtaIndex(double pta_low, double pta_high) {
  for (int index = 0; index < NPT1 + NPT1_ADD; index++) {
    if ( fabs(PT1_LO[index] - pta_low) < 0.001 && fabs(PT1_HI[index] - pta_high) < 0.001 ) return index;
  }
  std::cout << "Error GetPtaIndex(): This pta doesnot exist " << pta_low << "," << pta_high << std::endl;
  throw std::exception();
}
std::vector<int> GetPtaIndex (std::vector<std::pair<double, double>> input) {
  std::vector<int> ret;
  for (auto& pta_bin : input) {
    ret.push_back(GetPtaIndex(pta_bin.first, pta_bin.second));
  }
  return ret;
}
std::vector<int> GetPtaIndex (std::vector<double> input) {
  std::vector<int> ret;
  int size = input.size();
  for (int ibin = 0; ibin < size - 1; ibin++) {
    ret.push_back(GetPtaIndex(input[ibin], input[ibin + 1]));
  }
  return ret;
}
//---------------------------------------------------------------------------------------------------------------------



//---------------------------------------------------------------------------------------------------------------------
// 0,   1,   2,   3,   4,   5,   6,   7,   8,   9,  10,  11,  12,  13,  14,  15,  16,  17,  18,  19,  20,  21,  22,  23,  24,  25,  26,  27,  28,  29,  30,  31,  32,  33,  34,  35,  36
float PT2_LO[NPT2 + NPT2_ADD] =
{0.3, 0.4, 0.5, 0.6, 0.8, 1.0, 1.2, 1.4, 1.5, 1.6, 1.8, 2.0, 2.2, 2.4, 2.5, 2.6, 2.8, 3.0, 3.2, 3.4, 3.5, 3.6, 3.8, 4.0, 4.2, 4.4, 4.5, 4.6, 4.8, 5.0, 5.4, 5.5, 5.9, 6.0, 6.5, 7.0, 7.2};
float PT2_HI[NPT2 + NPT2_ADD] =
{0.4, 0.5, 0.6, 0.8, 1.0, 1.2, 1.4, 1.5, 1.6, 1.8, 2.0, 2.2, 2.4, 2.5, 2.6, 2.8, 3.0, 3.2, 3.4, 3.5, 3.6, 3.8, 4.0, 4.2, 4.4, 4.5, 4.6, 4.8, 5.0, 5.4, 5.5, 5.9, 6.0, 6.5, 7.0, 7.2, 8.0};

int pt2_add_lo[NPT2_ADD] = {0};
int pt2_add_up[NPT2_ADD] = {0};
void Initialize_Pt2Add() {
  std::vector<std::pair<double, double>> new_pt2_bins = {
    //  37,         38,         39,         40,         41,         42,         43,         44,        45,     46
    {0.5, 5.0}, {0.5, 1.0}, {1.0, 1.5}, {1.5, 2.0}, {2.0, 2.5}, {2.5, 3.0}, {3.0, 3.5}, {3.5, 4.0}, {4, 6}, {6, 8},
    //  47,         48,         49,
    {1.0, 2.0}, {2.0, 3.0}, {3.0, 4.0},
    //  50,         51,         52,         53
    {1.4, 1.6}, {2.4, 2.6}, {3.4, 3.6}, {4.4, 4.6}
  };

  if (new_pt2_bins.size() != NPT2_ADD) {
    std::cout << "Initialize_Pt2Add()::new_pt2_bins.size()!=NPT2_ADD " << new_pt2_bins.size() << "  " << NPT2_ADD << std::endl;
    throw std::exception();
  }

  int ibin = 0;
  for (auto new_bin : new_pt2_bins) {
    int low = -1, high = -1;
    for (int ipt2 = 0; ipt2 < NPT2; ipt2++) {
      if (fabs(PT2_LO[ipt2] - new_bin.first ) < 0.001 ) low = ipt2;
      if (fabs(PT2_HI[ipt2] - new_bin.second) < 0.001 ) high = ipt2 + 1;
    }
    if (low == -1 || high == -1 || low >= high) {
      std::cout << "Initialize_Pt2Add():: Problem adding new bin" << std::endl;
      throw std::exception();
    }

    pt2_add_lo[ibin] = low;
    pt2_add_up[ibin] = high;
    PT2_LO[NPT2 + ibin] = new_bin.first;
    PT2_HI[NPT2 + ibin] = new_bin.second;
    ibin++;
  }

  //checks
  //make sure edge of PT2_LO matches edge of previous PT2_HI for original NPT2 bins
  for (int ipt2 = 1; ipt2 < NPT2; ipt2++) {
    if (PT2_LO[ipt2] != PT2_HI[ipt2 - 1]) {
      std::cout << "Error in (PT2_LO,PT2_HI) original at index " << ipt2 << std::endl;
      throw std::exception();
    }
  }
  //make sure that no duplicate pt2 bins are produced
  for (int ipt2 = NPT2; ipt2 < NPT2 + NPT2_ADD; ipt2++) {
    if (GetPtbIndex(PT2_LO[ipt2], PT2_HI[ipt2]) != ipt2) {
      std::cout << "Duplicate PT2 bin found at index " << ipt2 << std::endl;
      throw std::exception();
    }
  }
}
std::string label_ptb2(int ipt, int correlation_type) {
  std::string                                                  label1 = "#it{b}";
  if (correlation_type == DataSetEnums::HADRON_JET_CORRELATIONS) label1 = "#it{h-jet}";

  sprintf(label, "#it{p}_{T}^{%s}#in(%.1f,%.1f)GeV", label1.c_str(), PT2_LO[ipt], PT2_HI[ipt]);
  std::string ret = label;
  return ret;
}
std::string label_ptb(int ipt, int correlation_type) {
  std::string                                                  label1 = "#it{b}";
  if (correlation_type == DataSetEnums::HADRON_JET_CORRELATIONS) label1 = "#it{h-jet}";

  sprintf(label, "%.1f<#it{p}_{T}^{%s}<%.1f GeV", PT2_LO[ipt], label1.c_str(), PT2_HI[ipt]);
  if (PT2_LO[ipt] == int(PT2_LO[ipt]))   sprintf(label, "%d<#it{p}_{T}^{%s}<%.1f GeV", int(PT2_LO[ipt]), label1.c_str(),   (PT2_HI[ipt]));
  if (PT2_HI[ipt] == int(PT2_HI[ipt]))   sprintf(label, "%.1f<#it{p}_{T}^{%s}<%d GeV",   (PT2_LO[ipt]), label1.c_str(), int(PT2_HI[ipt]));
  if (PT2_LO[ipt] == int(PT2_LO[ipt])
      && PT2_HI[ipt] == int(PT2_HI[ipt]))   sprintf(label, "%d<#it{p}_{T}^{%s}<%d GeV"  , int(PT2_LO[ipt]), label1.c_str(), int(PT2_HI[ipt]));

  std::string ret = label;
  return ret;
}
int GetPtbIndex(double ptb_low, double ptb_high) {
  for (int index = 0; index < NPT2 + NPT2_ADD; index++) {
    if (ptb_low == 0) return 999;
    if ( fabs(PT2_LO[index] - ptb_low) < 0.001 && fabs(PT2_HI[index] - ptb_high) < 0.001 ) return index;
  }
  std::cout << "Error GetPtbIndex(): This ptb doesnot exist " << ptb_low << "," << ptb_high << std::endl;
  throw std::exception();
}
std::vector<int> GetPtbIndex (std::vector<std::pair<double, double>> input) {
  std::vector<int> ret;
  for (auto& ptb_bin : input) {
    ret.push_back(GetPtbIndex(ptb_bin.first, ptb_bin.second));
  }
  return ret;
}
std::vector<int> GetPtbIndex (std::vector<double> input) {
  std::vector<int> ret;
  int size = input.size();
  for (int ibin = 0; ibin < size - 1; ibin++) {
    ret.push_back(GetPtbIndex(input[ibin], input[ibin + 1]));
  }
  return ret;
}
std::pair<double, double> GetPtbVals(int ipt) {
  return {PT2_LO[ipt], PT2_HI[ipt]};
}
//---------------------------------------------------------------------------------------------------------------------




//---------------------------------------------------------------------------------------------------------------------
/*-----------------------------------------------------------------------------
 * Combined binning for pTa and pTb
 *-----------------------------------------------------------------------------*/
int GetPtbIndexForPtaIndex(int ipta) {
  return GetPtbIndex(PT1_LO[ipta], PT1_HI[ipta]);
}
//int GetPtaIndexForPtbIndex(int iptb){
//  return GetPtaIndex(PT2_LO[iptb],PT2_HI[iptb]);
//}
std::string label_ptab(int ipta, int iptb, int correlation_type) {
  std::string                                                     label1 = "#it{a}", label2 = "#it{b}";
  if (correlation_type == DataSetEnums::HADRON_JET_CORRELATIONS) {label1 = "#it{h}", label2 = "#it{h-jet}";}

  if ( fabs(PT1_LO[ipta] - PT2_LO[iptb]) > 0.001 || fabs(PT1_HI[ipta] - PT2_HI[iptb]) > 0.001 ) {
    std::cout << "PT1 and PT2 bins dont correspond, ipta=" << ipta << "  iptb=" << iptb << std::endl;
    throw std::exception();
  }

  sprintf(label, "%.1f<#it{p}_{T}^{%s,%s}<%.1f GeV", PT1_LO[ipta], label1.c_str(), label2.c_str(), PT1_HI[ipta]);
  if (PT1_LO[ipta] == int(PT1_LO[ipta]))   sprintf(label, "%d<#it{p}_{T}^{%s,%s}<%.1f GeV", int(PT1_LO[ipta]), label1.c_str(), label2.c_str(),   (PT1_HI[ipta]));
  if (PT1_HI[ipta] == int(PT1_HI[ipta]))   sprintf(label, "%.1f<#it{p}_{T}^{%s,%s}<%d GeV",   (PT1_LO[ipta]), label1.c_str(), label2.c_str(), int(PT1_HI[ipta]));
  if (PT1_LO[ipta] == int(PT1_LO[ipta])
      && PT1_HI[ipta] == int(PT1_HI[ipta]))   sprintf(label, "%d<#it{p}_{T}^{%s,%s}<%d GeV"  , int(PT1_LO[ipta]), label1.c_str(), label2.c_str(), int(PT1_HI[ipta]));

  std::string ret = label;
  return ret;
}
//---------------------------------------------------------------------------------------------------------------------





//BINS
int ch_add_lo[NCH_ADD] = {0};
int ch_add_up[NCH_ADD] = {2};


//LABELS
float ETA_LO[NETA] = {0.0, 2.0, 2.5, 3.0, 3.5, 4.0,  2.0, 2.0, 1.0, 2.0, 3.0};
float ETA_HI[NETA] = {2.0, 5.0, 5.0, 5.0, 5.0, 5.0,  4.0, 4.5, 2.0, 3.0, 4.0};
//BINS
std::string label_eta(int ideta) {
  sprintf(label, "%.1f<|#Delta#eta|<%.1f", ETA_LO[ideta], ETA_HI[ideta]);
  if (ETA_LO[ideta] == int(ETA_LO[ideta]) && ETA_HI[ideta] == int(ETA_HI[ideta])) {
    sprintf(label, "%d<|#Delta#eta|<%d", int(ETA_LO[ideta]), int(ETA_HI[ideta]));
  }

  std::string ret = label;
  return ret;
}
int GetDetaIndex(double deta_low, double deta_high) {
  for (int index = 0; index < NETA; index++) {
    if ( fabs(ETA_LO[index] - deta_low) < 0.001 && fabs(ETA_HI[index] - deta_high) < 0.001 ) return index;
  }
  std::cout << "This deta doesnot exist " << deta_low << "," << deta_high << std::endl;
  throw std::exception();
}
std::vector<int> GetDetaIndex (std::vector<std::pair<double, double>> input) {
  std::vector<int> ret;
  for (auto& deta_bin : input) {
    ret.push_back(GetDetaIndex(deta_bin.first, deta_bin.second));
  }
  return ret;
}




//------------------------------------------------------------------------------
int GetPtBin1(float pt) {
  for (int i = 0; i < NPT1; i++) {
    if (pt >= PT1_LO[i] && pt < PT1_HI[i]) return i;
  }
  return -1;
}


int GetPtBin2(float pt) {
  for (int i = 0; i < NPT2; i++) {
    if (pt >= PT2_LO[i] && pt < PT2_HI[i]) return i;
  }
  return -1;
}
//------------------------------------------------------------------------------



//------------------------------------------------------------------------------
//Returns the vnn and its stat error for a given index
std::pair<float, float> GetVnn(int icent, int ipt1, int ipt2, int ich, int ideta, int ihar, int icent_periph, TFile *FourierFile) {
  std::pair<float, float> ret;
  char name[600];

  if (icent_periph == Bins::NO_PERIPHERAL_BIN) sprintf(name, "h_v%d%d_pta%d_ptb%.2d_ch%d_deta%.2d"             , ihar, ihar,               ipt1, ipt2, ich, ideta);
  else                                         sprintf(name, "h_v%d%d_pericent%.2d_pta%d_ptb%.2d_ch%d_deta%.2d", ihar, ihar, icent_periph, ipt1, ipt2, ich, ideta);
  TH1* hVnn = (TH1*)FourierFile->Get(name);
  if (!Common::CheckObject(hVnn, name, FourierFile)) throw std::exception();

  ret.first    = hVnn->GetBinContent(icent + 1);
  ret.second   = hVnn->GetBinError  (icent + 1);
  return ret;
}


//Returns the vn{ptb} and its stat error for a given index
std::pair<float, float> GetVnPtb(int icent, int ipt1, int ipt2, int ich, int ideta, int ihar, int icent_periph, TFile *FourierFile1, TFile *FourierFile2) {
  static TH1D *h_vnn = nullptr, *h_vnn_diag = nullptr;
  if (!h_vnn) {
    h_vnn     = new TH1D(Common::UniqueName().c_str(), "", 1, 0, 1);
    h_vnn_diag = new TH1D(Common::UniqueName().c_str(), "", 1, 0, 1);
  }


  int ipt2_for_ipt1 = GetPtbIndex(PT1_LO[ipt1], PT1_HI[ipt1]);
  std::pair<float, float> vnn_diag = GetVnn(icent, ipt1, ipt2_for_ipt1, ich, ideta, ihar, icent_periph, FourierFile2);
  h_vnn_diag->SetBinContent(1, vnn_diag.first);
  h_vnn_diag->SetBinError  (1, vnn_diag.second);
  Common::Take_Sqrt(h_vnn_diag);


  //for diagonal input return sqrt
  if (ipt2 == ipt2_for_ipt1 && FourierFile2 == FourierFile1) {
    std::pair<float, float> ret_diag;
    ret_diag.first    = h_vnn_diag->GetBinContent(1);
    ret_diag.second   = h_vnn_diag->GetBinError  (1);
    return ret_diag;
  }
  //for off-diagonal input
  else {
    std::pair<float, float> vnn = GetVnn(icent, ipt1, ipt2, ich, ideta, ihar, icent_periph, FourierFile1);
    h_vnn->SetBinContent(1, vnn.first);
    h_vnn->SetBinError  (1, vnn.second);


    std::pair<float, float> ret;
    h_vnn->Divide(h_vnn_diag);
    ret.first    = h_vnn->GetBinContent(1);
    ret.second   = h_vnn->GetBinError  (1);
    return ret;
  }
}
//------------------------------------------------------------------------------





//------------------------------------------------------------------------------
//Create a centrality dependence histogram from vector of bin-numbers
TH1D* CentdepHist(std::vector<int>centbins, std::string name) {
  double Xbins[1000] = {0.0};
  int NXbins = centbins.size();

  for (int ibin = 0; ibin < NXbins; ibin++) {
    Xbins[ibin] = NTRACK_PPB_LO[centbins[ibin]];
    if (ibin > 0) {
      if (Xbins[ibin] != NTRACK_PPB_HI[centbins[ibin - 1]]) {
        std::cout << "Error CentdepHist(): higher edge of lower bin doesnot match lower edge of next bin "
                  << ibin << "  " << centbins[ibin] << "  " << centbins[ibin - 1] << std::endl;
        throw std::exception();
      }
    }
  }
  Xbins[NXbins] = NTRACK_PPB_HI[centbins[NXbins - 1]];

  if (Xbins[NXbins] - Xbins[NXbins - 1] > 300) Xbins[NXbins] = Xbins[NXbins - 1] + 10; // Adjust top edge of last bin
  if (Xbins[     1] - Xbins[0         ] > 300) Xbins[0     ] =                      0; // Adjust lower edge of first bin

  TH1D* hist = new TH1D(name.c_str(), "", NXbins, Xbins);
  hist->GetXaxis()->SetTitle("#it{N}_{ch}^{rec}");

  return hist;
}
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
//Create a pta dependence histogram from vector of bin-numbers
TH1D* PtadepHist(std::vector<int>ipt1_vec, std::string name) {
  double Xbins[1000] = {0.0};
  int NXbins = ipt1_vec.size();

  for (int ibin = 0; ibin < NXbins; ibin++) {
    Xbins[ibin] = PT1_LO[ipt1_vec[ibin]];
    if (ibin > 0) {
      if (Xbins[ibin] != PT1_HI[ipt1_vec[ibin - 1]]) {
        std::cout << "Error PtadepHist(): higher edge of lower bin doesnot match lower edge of next bin "
                  << ibin << "  " << ipt1_vec[ibin] << "  " << ipt1_vec[ibin - 1] << std::endl;
        throw std::exception();
      }
    }
  }
  Xbins[NXbins] = PT1_HI[ipt1_vec[NXbins - 1]];
  TH1D* hist = new TH1D(name.c_str(), "", NXbins, Xbins);
  hist->GetXaxis()->SetTitle("#it{p}_{T}^{#it{a}} [GeV]");
  return hist;
}

//Create a ptb dependence histogram from vector of bin-numbers
TH1D* PtbdepHist(std::vector<int>ipt2_vec, std::string name, int correlation_type) {
  double Xbins[1000] = {0.0};
  int NXbins = ipt2_vec.size();

  for (int ibin = 0; ibin < NXbins; ibin++) {
    Xbins[ibin] = PT2_LO[ipt2_vec[ibin]];
    if (Xbins[ibin] < 0.1 && Xbins[ibin] > -1e-10) Xbins[ibin] = 0.0;
    if (ibin > 0) {
      if (Xbins[ibin] != PT2_HI[ipt2_vec[ibin - 1]] && ipt2_vec[ibin - 1] != 999) {
        std::cout << __PRETTY_FUNCTION__ << ":: Error higher edge of lower bin doesnot match lower edge of next bin "
                  << ibin << "  " << ipt2_vec[ibin] << "  " << ipt2_vec[ibin - 1] << std::endl;
        throw std::exception();
      }
    }
  }
  Xbins[NXbins] = PT2_HI[ipt2_vec[NXbins - 1]];
  TH1D* hist = new TH1D(name.c_str(), "", NXbins, Xbins);
  hist->GetXaxis()->SetTitle("#it{p}_{T}^{#it{b}} [GeV]");
  if (correlation_type == DataSetEnums::HADRON_JET_CORRELATIONS)hist->GetXaxis()->SetTitle("#it{p}_{T}^{#it{h-jet}} [GeV]");
  return hist;
}
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
//Create a Deta dependence histogram from vector of bin-numbers
TH1D* DetadepHist(std::vector<int>deta_vec, std::string name) {
  double Xbins[1000] = {0.0};
  int NXbins = deta_vec.size();

  for (int ibin = 0; ibin < NXbins; ibin++) {
    Xbins[ibin] = ETA_LO[deta_vec[ibin]];
    if (ibin > 0) {
      if (Xbins[ibin] != ETA_HI[deta_vec[ibin - 1]]) {
        std::cout << "Error DetadepHist(): higher edge of lower bin doesnot match lower edge of next bin "
                  << ibin << "  " << deta_vec[ibin] << "  " << deta_vec[ibin - 1] << std::endl;
        throw std::exception();
      }
    }
  }
  Xbins[NXbins] = ETA_HI[deta_vec[NXbins - 1]];
  TH1D* hist = new TH1D(name.c_str(), "", NXbins, Xbins);
  hist->GetXaxis()->SetTitle("#Delta#eta");
  return hist;
}
//------------------------------------------------------------------------------










//------------------------------------------------------------------------------
//These functions are used to restrict the values of the different deta,pt,centrality...
//bins that we loop over in the main code
//For the defaults cases we should typically loop over all bins
//but for most cross-checks only looping over a few bins is necessary


std::vector<int> CentBins(ACCEPT_SETTINGS) {
  std::vector<int> full_set_cent;
  for (int icent = 16; icent < NCENT + NCENT_ADD; icent++) full_set_cent.push_back(icent);
  return full_set_cent;
}

std::vector<int> CentBinsPeriph(ACCEPT_SETTINGS) {
  std::vector<int> centbins_peripheral;
  centbins_peripheral = {
    Bins::GetCentIndex( 0, 20),
    Bins::GetCentIndex(10, 30),
    Bins::GetCentIndex(10, 20),
    // Bins::GetCentIndex(20, 30),
  };
  return centbins_peripheral;
}

std::vector<int> PtaBins (ACCEPT_SETTINGS) {
  std::vector<int> full_set_pta;
  std::vector<int> min_set_pta = {GetPtaIndex(0.5, 5.0)};
  for (int ipt1 = 0; ipt1 < NPT1 + NPT1_ADD; ipt1++) full_set_pta.push_back(ipt1);
  return min_set_pta;
}

std::vector<int> PtbBins (ACCEPT_SETTINGS) {
  std::vector<int> full_set_ptb;
  std::vector<int> min_set_ptb = GetPtbIndex( {{0.5, 5.0}, {0.5, 1.0}, {1.0, 1.5}, {1.5, 2.0}, {2.0, 2.5}, {2.5, 3.0}, {3.0, 3.5}, {3.5, 4.0}, {4, 6}, {6, 8}} );
  // std::vector<int> min_set_ptb = {GetPtbIndex(0.5, 5.0)};
  for (int ipt2 = 0; ipt2 < NPT2 + NPT2_ADD; ipt2++) full_set_ptb.push_back(ipt2);
  return full_set_ptb;
  // return min_set_ptb;
}

std::vector<int> DetaBins(ACCEPT_SETTINGS) {
  std::vector<int> min_set_deta = {GetDetaIndex(2.0, 5.0)};//, GetDetaIndex(2.0, 4.5), GetDetaIndex(2.0, 4.0)};
  std::vector<int> full_set_deta;
  for (int ideta = 0; ideta < NETA; ideta++) full_set_deta.push_back(ideta);
  return min_set_deta;
}

std::vector<int> ChBins(ACCEPT_SETTINGS) {
  std::vector<int> min_set_ch = {2};
  std::vector<int> full_set_ch = {0, 1, 2};
  return min_set_ch;
}
//------------------------------------------------------------------------------



int Initialize() {
  std::cout << "Called Bins::Initialize()" << std::endl;
  Initialize_CentAdd();
  Initialize_Pt1Add();
  Initialize_Pt2Add();
  return 1;
}
int m_is_initialized = Initialize();





};//namespace Bins

#endif
