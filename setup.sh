# setupATLAS
# lsetup 'root 6.16.00-x86_64-slc6-gcc62-opt'
# lsetup 'sft releases/LCG_87/MCGenerators/lhapdf/6.1.6'
# lsetup 'sft releases/LCG_87/HepMC/2.06.09'
# lsetup 'sft releases/LCG_87/fastjet/3.2.0'

# ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
# source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh ""
# localSetupROOT 6.16.00-x86_64-slc6-gcc62-opt
# lsetup 'sft releases/LCG_87/MCGenerators/lhapdf/6.1.6'
# lsetup 'sft releases/LCG_87/HepMC/2.06.09'
# lsetup 'sft releases/LCG_87/fastjet/3.2.0'

# export PYTHIA8_DIR=/usatlas/u/pyin/workarea/PYTHIA8/pythia8230
# export LD_LIBRARY_PATH=$PYTHIA8_DIR/lib:$LD_LIBRARY_PATH
# export LHAPDF_DATA_PATH=/cvmfs/sft.cern.ch/lcg/external/lhapdfsets/current:$SFT_HOME_lhapdf/share/LHAPDF
# export PATH=$PYTHIA8_DIR/bin:$PATH

# source /usatlas/u/pyin/workarea/PYTHIA8/config.sh


ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh ""
lsetup "views LCG_102 x86_64-centos7-gcc11-opt"

export LD_LIBRARY_PATH=$PYTHIA8/lib:$LD_LIBRARY_PATH
export PATH=$PYTHIA8/bin:$PATH
