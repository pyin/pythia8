cd pythia8230
./configure \
--prefix=$PWD/../pythia-install \
--with-lhapdf6=$SFT_HOME_lhapdf \
--with-lhapdf6-plugin=LHAPDF6.h \
--with-boost=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/boost/boost-1.47.0-python2.6-x86_64-slc5-gcc43/boost-1.47.0-python2.6-x86_64-slc5-gcc43 \
--with-root=$ROOTSYS \
--lcg=$rootCmtConfig $python_string \
--cxx-common='-std=c++11 -pedantic -W -Wall -Wshadow -fPIC'

gmake clean
gmake -j24
gmake install

cd ..
